/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package dmax.dialog;

public final class R {
    private R() {}

    public static final class attr {
        private attr() {}

        public static final int DialogSpotColor = 0x7f030000;
        public static final int DialogSpotCount = 0x7f030001;
        public static final int DialogTitleAppearance = 0x7f030002;
        public static final int DialogTitleText = 0x7f030003;
    }
    public static final class color {
        private color() {}

        public static final int spots_dialog_color = 0x7f0500a4;
    }
    public static final class dimen {
        private dimen() {}

        public static final int progress_margin = 0x7f0600f5;
        public static final int progress_width = 0x7f0600f6;
        public static final int spot_size = 0x7f0600f7;
        public static final int title_margin = 0x7f060104;
    }
    public static final class drawable {
        private drawable() {}

        public static final int dmax_spots_spot = 0x7f070079;
    }
    public static final class id {
        private id() {}

        public static final int dmax_spots_progress = 0x7f08005c;
        public static final int dmax_spots_title = 0x7f08005d;
    }
    public static final class layout {
        private layout() {}

        public static final int dmax_spots_dialog = 0x7f0b0039;
    }
    public static final class style {
        private style() {}

        public static final int SpotsDialogDefault = 0x7f0f0133;
    }
    public static final class styleable {
        private styleable() {}

        public static final int[] Dialog = { 0x10100f4, 0x10100f5, 0x7f030000, 0x7f030001, 0x7f030002, 0x7f030003, 0x7f0300d6, 0x7f0300d7, 0x7f0300d8, 0x7f0300d9, 0x7f0300da, 0x7f0300db, 0x7f0300dc, 0x7f0300de, 0x7f0300df, 0x7f0300e0, 0x7f0300e1, 0x7f0300e2, 0x7f0300e3, 0x7f0300e6, 0x7f0300e7, 0x7f0300e8, 0x7f0300e9, 0x7f0300ec, 0x7f0300ed, 0x7f0300ee, 0x7f0300ef, 0x7f0300f0, 0x7f0300f1, 0x7f0300f2, 0x7f0300f3, 0x7f0300f4, 0x7f0300f5, 0x7f0300f6, 0x7f0300f7, 0x7f0300f8, 0x7f0300fa, 0x7f0300fb };
        public static final int Dialog_android_layout_width = 0;
        public static final int Dialog_android_layout_height = 1;
        public static final int Dialog_DialogSpotColor = 2;
        public static final int Dialog_DialogSpotCount = 3;
        public static final int Dialog_DialogTitleAppearance = 4;
        public static final int Dialog_DialogTitleText = 5;
        public static final int Dialog_di_actionBackground = 6;
        public static final int Dialog_di_actionRipple = 7;
        public static final int Dialog_di_actionTextAppearance = 8;
        public static final int Dialog_di_actionTextColor = 9;
        public static final int Dialog_di_backgroundColor = 10;
        public static final int Dialog_di_cancelable = 11;
        public static final int Dialog_di_canceledOnTouchOutside = 12;
        public static final int Dialog_di_cornerRadius = 13;
        public static final int Dialog_di_dimAmount = 14;
        public static final int Dialog_di_dividerColor = 15;
        public static final int Dialog_di_dividerHeight = 16;
        public static final int Dialog_di_elevation = 17;
        public static final int Dialog_di_inAnimation = 18;
        public static final int Dialog_di_layoutDirection = 19;
        public static final int Dialog_di_maxElevation = 20;
        public static final int Dialog_di_maxHeight = 21;
        public static final int Dialog_di_maxWidth = 22;
        public static final int Dialog_di_negativeActionBackground = 23;
        public static final int Dialog_di_negativeActionRipple = 24;
        public static final int Dialog_di_negativeActionTextAppearance = 25;
        public static final int Dialog_di_negativeActionTextColor = 26;
        public static final int Dialog_di_neutralActionBackground = 27;
        public static final int Dialog_di_neutralActionRipple = 28;
        public static final int Dialog_di_neutralActionTextAppearance = 29;
        public static final int Dialog_di_neutralActionTextColor = 30;
        public static final int Dialog_di_outAnimation = 31;
        public static final int Dialog_di_positiveActionBackground = 32;
        public static final int Dialog_di_positiveActionRipple = 33;
        public static final int Dialog_di_positiveActionTextAppearance = 34;
        public static final int Dialog_di_positiveActionTextColor = 35;
        public static final int Dialog_di_titleTextAppearance = 36;
        public static final int Dialog_di_titleTextColor = 37;
    }
}
