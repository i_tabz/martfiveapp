package com.example.martfiveapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DownloadManager;
import android.database.DatabaseErrorHandler;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.martfiveapp.Common.Common;
import com.example.martfiveapp.Database.Database;
import com.example.martfiveapp.Model.Item;
import com.example.martfiveapp.Model.Order;
import com.example.martfiveapp.Model.Rating;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.listener.RatingDialogListener;

import java.util.Arrays;

public class Item_Detail extends AppCompatActivity implements RatingDialogListener {

    TextView item_name,item_price,item_description;
    ImageView item_image;
    CollapsingToolbarLayout collapsingToolbarLayout;
    FloatingActionButton btnCart,btnRating;
    RatingBar ratingBar;
    ElegantNumberButton numberButton;
    String itemId="";
    FirebaseDatabase database;
    DatabaseReference items;
    DatabaseReference ratingTbl;
    Item currentItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item__detail);

        //Firebase
        database=FirebaseDatabase.getInstance();
        items=database.getReference("Items");

        ratingTbl = database.getReference("Rating");


        //Init view
        numberButton=(ElegantNumberButton) findViewById(R.id.number_button);
        btnCart=(FloatingActionButton) findViewById(R.id.btnCart);
        btnRating=(FloatingActionButton) findViewById(R.id.btn_rating);
        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Database(getBaseContext()).addToCart(new Order(
                        itemId,
                        currentItem.getName(),
                        numberButton.getNumber(),
                        currentItem.getPrice()


                ));

                Toast.makeText(Item_Detail.this,"Added to Cart",Toast.LENGTH_SHORT).show();
            }
        });

        ratingBar=(RatingBar) findViewById(R.id.ratingBar);

        btnRating.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                showRatingDialog();
            }
        });

        item_description=(TextView)findViewById(R.id.item_description);
        item_name=(TextView)findViewById(R.id.item_name);
        item_price=(TextView)findViewById(R.id.item_price);
        item_image=(ImageView) findViewById(R.id.img_item);

        collapsingToolbarLayout=(CollapsingToolbarLayout) findViewById(R.id.collapsing);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppbar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppbar);

        //Get item id
        if(getIntent()!=null)
            itemId = getIntent().getStringExtra("ItemId");
        if(!itemId.isEmpty())
        {
            if(Common.isConnectedToInternet(getBaseContext())){
                getDetailItem(itemId);
                //getRatingItem(itemId);
            }else{
                Toast.makeText(Item_Detail.this,"please Check your Connection !!",Toast.LENGTH_SHORT).show();
                return;
            }
        }
        else
        {
            Toast.makeText(Item_Detail.this,"Item Id is empty !!",Toast.LENGTH_SHORT).show();
            return;
        }

    }

    private void getRatingItem(String itemId) {
        Query itemRating = ratingTbl.orderByChild("itemId").equalTo(itemId);
        itemRating.addValueEventListener(new ValueEventListener(){
            int count =0, sum = 0;
            @Override
            public void onDataChange(DataSnapshot dataSnapshot){
                for(DataSnapshot postSnapshot:dataSnapshot.getChildren()){
                    Rating item =postSnapshot.getValue(Rating.class);
                    sum += Integer.parseInt(item.getRateValue());
                    count++;
                }
                if(count != 0){
                    float average = sum/count;
                    ratingBar.setRating(average);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void showRatingDialog() {
        new AppRatingDialog.Builder()
                .setPositiveButtonText("Submit")
                .setNegativeButtonText("Cancel")
                .setNoteDescriptions(Arrays.asList("Very Bad","Not Good","Quite Ok","Very Good","Excellent"))
                .setDefaultRating(2)
                .setTitle("Rate this food")
                .setDescription("Please select some stars and give your feedback")
                .setTitleTextColor(R.color.colorPrimary)
                .setDescriptionTextColor(R.color.colorPrimaryDark)
                .setHint("Please write your comment here...")
                .setHintTextColor(android.R.color.white)
                .setCommentTextColor(android.R.color.white)
                .setCommentBackgroundColor(R.color.colorPrimaryDark)
                .setWindowAnimation(R.style.RatingDialogFadeAnim)
                .create(Item_Detail.this)
                .show();


    }

    private void getDetailItem(final String itemId){

        items.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot){
                //Check User Exist in Database
                if(dataSnapshot.child(itemId).exists())
                {
                    currentItem=dataSnapshot.child(itemId).getValue(Item.class);

                    Picasso.with(getBaseContext()).load( currentItem.getImage()).into(item_image);

                    collapsingToolbarLayout.setTitle( currentItem.getName());
                    item_price.setText( currentItem.getPrice());
                    item_name.setText( currentItem.getName());
                    item_description.setText( currentItem.getDescription());
                    ratingBar.setRating(Float.parseFloat( currentItem.getReview().toString()));
                }
                else{
                    Toast.makeText(Item_Detail.this, "Item does not exist!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void  onCancelled(DatabaseError databaseError){
                Toast.makeText(Item_Detail.this, "Failed to Connect Database!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onPositiveButtonClicked(int i,String s){
        /*final Rating rating=new Rating(Common.currentUser.getPhone(), itemId, String.valueOf(i), s);
        ratingTbl.child(Common.currentUser.getPhone()).addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(DataSnapshot dataSnapshot){
                if(dataSnapshot.child(Common.currentUser.getPhone()).exists()){
                    ratingTbl.child(Common.currentUser.getPhone()).removeValue();
                    ratingTbl.child(Common.currentUser.getPhone()).setValue(rating);
                }else{
                    ratingTbl.child(Common.currentUser.getPhone()).setValue(rating);
                }
                Toast.makeText(Item_Detail.this,"Thank You for rating us!!",Toast.LENGTH_SHORT).show();
            }
            public void onCancelled(DatabaseError databaseError){

            }
        });*/
    }

    @Override
    public void onNegativeButtonClicked() {

    }
}
