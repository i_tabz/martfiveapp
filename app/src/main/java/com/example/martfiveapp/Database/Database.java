package com.example.martfiveapp.Database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import java.util.ArrayList;
import java.util.List;
import android.database.Cursor;

import com.example.martfiveapp.Model.Order;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class Database extends SQLiteAssetHelper{
    private final static String DB_NAME="MartFiveDB.db";
    private final static int DB_VER=1;

    public Database(Context context) {
        super(context, DB_NAME,null,DB_VER);
    }

   /* public List<Order> getCarts() {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String[] sqlSelet = {"ProductName", "ProductId ", "Quantity ", "Price"};
        String sqlTable = "OrderDetail";

        qb.setTables(sqlTable);
        Cursor c = qb.query(db, sqlSelet, null, null, null, null, null);

        final List<Order> result = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                result.add(new Order(c.getString(c.getColumnIndex("ProductId")), c.getString(c.getColumnIndex("ProductName")),
                        c.getString(c.getColumnIndex("Quantity")), c.getString(c.getColumnIndex("Price"))
                ));
            } while (c.moveToNext());
        }
        return result;
    }*/

    public List<Order> getCarts()
    {
        SQLiteDatabase db = getReadableDatabase();
        String sqlTable = "OrderDetail";
        String query="SELECT * FROM "+ sqlTable;
        List<Order> list=new ArrayList<>();

        Cursor cursor=db.rawQuery(query,null);
        if(cursor.moveToFirst())
        {
            do{
                Order item =new Order();
                item.setID(cursor.getInt(cursor.getColumnIndex("ID")));
                item.setProductId(cursor.getString(cursor.getColumnIndex("ProductId")));
                item.setProductName(cursor.getString(cursor.getColumnIndex("ProductName")));
                item.setPrice(cursor.getString(cursor.getColumnIndex("Price")));
                item.setQuntity(cursor.getString(cursor.getColumnIndex("Quantity")));
                list.add(item);
            }while (cursor.moveToNext());

        }
        return list;
    }
    public void addToCart(Order order)
    {
        SQLiteDatabase db =getReadableDatabase();
        String query=String.format("INSERT INTO OrderDetail (ProductId,ProductName,Quantity,Price) VALUES ('%s','%s','%s','%s');",
                order.getProductId(),
                order.getProductName(),
                order.getQuntity(),
                order.getPrice());

        db.execSQL(query);
    }

    public void cleanCart()
    {
        SQLiteDatabase db =getReadableDatabase();
        String query=String.format("DELETE FROM OrderDetail");
        db.execSQL(query);
    }

    public int getCountCart() {
        int count=0;
        SQLiteDatabase db = getReadableDatabase();
        String query=String.format("SELECT COUNT(*) FROM  OrderDetail");
        Cursor cursor=db.rawQuery(query,null);
        if(cursor.moveToFirst())
        {
            do{
                count=cursor.getInt(0);
            }while(cursor.moveToNext());
        }
        return count;
    }


    public void updateCart(Order order) {

        SQLiteDatabase db = getReadableDatabase();
        String query=String.format("UPDATE OrderDetail SET Quantity=%s WHERE ID= %d",order.getQuntity(),order.getID());
        db.execSQL(query);
    }
}




