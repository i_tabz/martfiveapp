package com.example.martfiveapp.Model;

public class  User {
    private String Email;
    private String FirstName;
    private String LastName;
    private String Password;
    private String Phone;
    private String SecureCode;



    public String getSecureCode() {
        return SecureCode;
    }

    public void setSecureCode(String secureCode) {
        SecureCode = secureCode;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public User() {
    }

    public User(String email, String firstName, String lastName, String password,String secureCode) {
        Email = email;
        FirstName = firstName;
        LastName = lastName;
        Password = password;
        SecureCode=secureCode;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}

