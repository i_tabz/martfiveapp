package com.example.martfiveapp.Model;

public class Rating {
    private String userPhone;
    private String itemId;
    private String rateValue;
    private String review;

    public Rating(){

    }

    public Rating(String userPhone,String itemId,String rateValue,String review){
        this.userPhone=userPhone;
        this.itemId=itemId;
        this.rateValue=rateValue;
        this.review=review;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getRateValue() {
        return rateValue;
    }

    public void setRateValue(String rateValue) {
        this.rateValue = rateValue;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }
}
