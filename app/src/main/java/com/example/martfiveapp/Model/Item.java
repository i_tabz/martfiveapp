package com.example.martfiveapp.Model;

public class Item {
    private String CategoryId;
    private String Description;
    private String Image;
    private String Name;
    private String Price;
    private String Review;
    private String Quantity;

    public Item() {
    }

    public Item(String categoryId, String description, String image, String name, String price, String review, String quantity) {
        CategoryId = categoryId;
        Description = description;
        Image = image;
        Name = name;
        Price = price;
        Review = review;
        Quantity = quantity;
    }

    public String getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(String categoryId) {
        CategoryId = categoryId;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getReview() {
        return Review;
    }

    public void setReview(String review) {
        Review = review;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }
}
