package com.example.martfiveapp.Model;

public class Order {
    private int ID;
    private String ProductId;
    private String ProductName;
    private String Quantity;
    private String Price;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Order(int ID, String productId, String productName, String quantity, String price) {
        this.ID = ID;
        ProductId = productId;
        ProductName = productName;
        Quantity = quantity;
        Price = price;
    }

    public Order() {
    }

    public Order(String productId, String productName, String quntity, String price) {
        ProductId = productId;
        ProductName = productName;
        Quantity = quntity;
        Price = price;
    }



    public String getQuntity() {
        return Quantity;
    }

    public void setQuntity(String quntity) {
        Quantity = quntity;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }
}

