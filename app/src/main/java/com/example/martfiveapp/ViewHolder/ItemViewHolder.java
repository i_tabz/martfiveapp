package com.example.martfiveapp.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.martfiveapp.Interface.ItemClickListener;
import com.example.martfiveapp.R;

public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView item_name;
    public TextView item_price;
    public TextView item_quantity;
    public ImageView item_image;

    private ItemClickListener itemClickListener;

    public ItemViewHolder(@NonNull View itemView) {
        super(itemView);
        item_name = (TextView) itemView.findViewById(R.id.item_name);
        item_price = (TextView) itemView.findViewById(R.id.item_price);
        item_quantity = (TextView) itemView.findViewById(R.id.item_quantity);
        item_image = (ImageView) itemView.findViewById(R.id.item_image);

        itemView.setOnClickListener(this);
    }
    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {

        itemClickListener.onClick(v, getAdapterPosition(),false);
    }
}
