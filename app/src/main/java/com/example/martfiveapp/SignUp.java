package com.example.martfiveapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.martfiveapp.Common.Common;
import com.example.martfiveapp.Model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class SignUp extends AppCompatActivity {
    EditText phone,firstName,lastName,pass,corrPass,email,scrCode;
    Button btnSignUp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        phone = (EditText)findViewById(R.id.phone);
        firstName = (EditText)findViewById(R.id.FirstName);
        email = (EditText)findViewById(R.id.email);
        lastName = (EditText)findViewById(R.id.LastName);
        pass = (EditText)findViewById(R.id.password);
        corrPass = (EditText)findViewById(R.id.conPass);
        scrCode = (EditText)findViewById(R.id.secureCode);
        btnSignUp = (Button) findViewById(R.id.signup);
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_user= database.getReference("User");

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( phone.getText().toString().length() != 0 ) {
                    if (firstName.getText().toString().length() != 0) {
                        if (lastName.getText().toString().length() != 0) {
                            if (pass.getText().toString().length() != 0){
                                if( corrPass.getText().toString().length() != 0 ) {
                                    if(pass.getText().toString().matches(corrPass.getText().toString()))
                                    {
                                        if(Common.isConnectedToInternet(getBaseContext())){
                                            final ProgressDialog mDialog = new ProgressDialog(SignUp.this);
                                            mDialog.setMessage("Waiting...");
                                            mDialog.show();
                                            table_user.addValueEventListener(new ValueEventListener()
                                            {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                //Check User Exist in Database
                                                if(dataSnapshot.child(phone.getText().toString()).exists()) {
                                                    mDialog.dismiss();
                                                    Toast.makeText(SignUp.this, "Phone Number Already Exist!", Toast.LENGTH_SHORT).show();
                                                }
                                                else{
                                                    mDialog.dismiss();
                                                    User user=new User(email.getText().toString(),firstName.getText().toString(),lastName.getText().toString(),pass.getText().toString(),scrCode.getText().toString());
                                                    table_user.child(phone.getText().toString()).setValue(user);
                                                    Toast.makeText(SignUp.this, "SuccessFully Added!", Toast.LENGTH_SHORT).show();
                                                    finish();
                                                }
                                            }
                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                                mDialog.dismiss();
                                                Toast.makeText(SignUp.this, "Failed to Connect Database!", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        }else{
                                               Toast.makeText(SignUp.this,"please Check your Connection !!",Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                    }
                                    else {
                                        corrPass.setError("Password doesn't match");
                                    }
                                }else
                                    corrPass.setError( "Confirm Password is required!" );
                            }else
                                pass.setError( "Password is required!" );
                        } else
                            lastName.setError("Last Name is required!");
                    } else
                        firstName.setError("First Name is required!");
                } else
                    phone.setError( "Phone Number is required!" );
            }
        });

    }
}
