package com.example.martfiveapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.martfiveapp.Common.Common;
import com.example.martfiveapp.Interface.ItemClickListener;
import com.example.martfiveapp.Model.Request;
import com.example.martfiveapp.ViewHolder.CategoryViewHolder;
import com.example.martfiveapp.ViewHolder.OrderViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class OrdersStatus extends AppCompatActivity {

    public RecyclerView recyclerView;
    public RecyclerView.LayoutManager layoutManager;

    private RecyclerView mPeopleRV;
    private FirebaseRecyclerAdapter<Request, OrderViewHolder> mPeopleRVAdapter;

    FirebaseRecyclerAdapter<Request, OrderViewHolder>adapter;
    FirebaseDatabase database;
    DatabaseReference request;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_status);

        database=FirebaseDatabase.getInstance();
        request=database.getReference("Requests");
        recyclerView= (RecyclerView)findViewById(R.id.listOrders);
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        loadOrders(Common.currentUser.getPhone());
    }
    private void loadOrders(String phone){

        FirebaseRecyclerOptions<Request> options =
                new FirebaseRecyclerOptions.Builder<Request>()
                        .setQuery(request.orderByChild("phone").equalTo(phone), Request.class)
                        .build();

         adapter=new FirebaseRecyclerAdapter<Request, OrderViewHolder>(options) {
             @Override
             protected void onBindViewHolder(@NonNull OrderViewHolder orderViewHolder, int i, @NonNull Request request) {
                 if(request != null) {
                     orderViewHolder.txtOrderId.setText(adapter.getRef(i).getKey());
                     orderViewHolder.txtOrderPhone.setText(request.getPhone());
                     //orderViewHolder.txtOrderStatus.setText(convertCodeToStatus(request.getStatus()));
                     orderViewHolder.txtOrderAddress.setText(request.getAddress());
                     orderViewHolder.setItemClickListener(new ItemClickListener() {
                         @Override
                         public void onClick(View view, int position, boolean isLongClick) {
                             //Get CategoryId and Send it to new Activity
                             Toast.makeText(OrdersStatus.this,"Order is Clicked :p",Toast.LENGTH_SHORT).show();
                         }
                     });
                 }
                 else
                 {
                     Toast.makeText(OrdersStatus.this,"No Order in DB",Toast.LENGTH_SHORT).show();
                 }
             }
             @NonNull
             @Override
             public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                 View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_layout, parent, false);
                 return new OrderViewHolder(view);
             }
         };
        recyclerView.setAdapter(adapter);
        adapter.startListening();
    }
    private String convertCodeToStatus(String status){
        if(status.equals("0"))
            return "Placed";
        else if(status.equals("1"))
            return "On my way";
        else
            return "Shipped";
    }
}