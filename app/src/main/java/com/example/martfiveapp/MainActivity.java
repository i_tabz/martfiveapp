package com.example.martfiveapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.martfiveapp.Common.Common;
import com.example.martfiveapp.Model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import io.paperdb.Paper;

public class MainActivity extends AppCompatActivity {

    TextView txtSignUp ,txtForgetPass;
    EditText phone,pass;
    Button btnLoginIn;
    CheckBox chkRemember;
    FirebaseDatabase database;
    DatabaseReference table_user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        phone=(EditText)findViewById(R.id.phone);
        pass=(EditText)findViewById(R.id.password);
        btnLoginIn=(Button) findViewById(R.id.login);
        chkRemember=(CheckBox) findViewById(R.id.remember);
        txtForgetPass=(TextView) findViewById(R.id.forgotPass);

        database = FirebaseDatabase.getInstance();
        table_user= database.getReference("User");

        //Init Paper
        Paper.init(this);

        txtForgetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               showForgetPasswordDialog();
            }
        });

        btnLoginIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( phone.getText().toString().length() != 0 ) {
                    if (pass.getText().toString().length() != 0)
                    {
                        if(Common.isConnectedToInternet(getBaseContext())){

                            //Save user and Password
                            if(chkRemember.isChecked())
                            {
                                Paper.book().write(Common.USER_KEY,phone.getText().toString());
                                Paper.book().write(Common.PWD_KEY,pass.getText().toString());
                            }

                            final ProgressDialog mDialog = new ProgressDialog(MainActivity.this);
                            mDialog.setMessage("Waiting...");
                            mDialog.show();
                            table_user.addValueEventListener(new ValueEventListener()
                            {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    //Check User Exist in Database
                                    if(dataSnapshot.child(phone.getText().toString()).exists()) {

                                        //Get User Information
                                        mDialog.dismiss();
                                        User user = dataSnapshot.child(phone.getText().toString()).getValue(User.class);
                                        user.setPhone(phone.getText().toString());//set Phone
                                        if (user.getPassword().equals(pass.getText().toString()))
                                        {

                                            if(Common.isConnectedToInternet(getBaseContext())){
                                                Intent homeIntent = new Intent(MainActivity.this,Home.class);
                                                Common.currentUser = user;
                                                startActivity(homeIntent);
                                                finish();
                                            }else{
                                                Toast.makeText(MainActivity.this,"please Check your Connection !!",Toast.LENGTH_SHORT).show();
                                                return;
                                            }
                                        }else {
                                            Toast.makeText(MainActivity.this, "InCorrect Password !!!", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    else{
                                        mDialog.dismiss();
                                        Toast.makeText(MainActivity.this, "User not Exist!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    mDialog.dismiss();
                                    Toast.makeText(MainActivity.this, "Failed to Connect Database!", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        else{
                            Toast.makeText(MainActivity.this,"please Check your Connection !!",Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else
                        pass.setError( "Password is required!" );
                } else
                    phone.setError( "Phone Number is required!" );
            }
        });
        //Check Remember
        String user = Paper.book().read(Common.USER_KEY);
        String pwd = Paper.book().read(Common.PWD_KEY);
        if(user != null && pwd != null)
        {
            if(!user.isEmpty() && !pwd.isEmpty())
            {
                login(user,pwd);
            }
        }
    }

    private void login(final String phone, final String pwd) {
        //just copy login code
        if(Common.isConnectedToInternet(getBaseContext())){

            final ProgressDialog mDialog = new ProgressDialog(MainActivity.this);
            mDialog.setMessage("Waiting...");
            mDialog.show();
            table_user.addValueEventListener(new ValueEventListener()
            {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    //Check User Exist in Database
                    if(dataSnapshot.child(phone).exists()) {

                        //Get User Information
                        mDialog.dismiss();
                        User user = dataSnapshot.child(phone).getValue(User.class);
                        if (user.getPassword().equals(pwd))
                        {

                            if(Common.isConnectedToInternet(getBaseContext())){
                                Intent homeIntent = new Intent(MainActivity.this,Home.class);
                                Common.currentUser = user;
                                startActivity(homeIntent);
                                finish();
                            }else{
                                Toast.makeText(MainActivity.this,"please Check your Connection !!",Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }else {
                            Toast.makeText(MainActivity.this, "InCorrect Password !!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        mDialog.dismiss();
                        Toast.makeText(MainActivity.this, "User not Exist!", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    mDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Failed to Connect Database!", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else{
            Toast.makeText(MainActivity.this,"please Check your Connection !!",Toast.LENGTH_SHORT).show();
            return;
        }
    }

    private void showForgetPasswordDialog() {

        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Forgot Password");
        builder.setMessage("Enter your secure code");
        LayoutInflater inflater=this.getLayoutInflater();
        View forgot_view=inflater.inflate(R.layout.activity_forget_password,null);
        builder.setView(forgot_view);
        builder.setIcon(R.drawable.ic_security_orange_24dp);
        final MaterialEditText edtPhone=(MaterialEditText)forgot_view.findViewById(R.id.txtPhoneNo);
        final MaterialEditText edtSecureCode=(MaterialEditText)forgot_view.findViewById(R.id.txtSecureCode);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                table_user.addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.child(edtPhone.getText().toString()).exists()) {

                            User user = dataSnapshot.child(edtPhone.getText().toString()).getValue(User.class);
                            user.setPhone(edtPhone.getText().toString());
                            String u = user.getSecureCode();
                            if (u != null) {
                                if (u.equals(edtSecureCode.getText().toString())) {
                                    Toast.makeText(MainActivity.this, "Your password :" + user.getPassword(), Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(MainActivity.this, "Wrong secure code!", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(MainActivity.this, "Maazarat!!! There is some issue in DB!", Toast.LENGTH_LONG).show();
                            }
                        }
                        else{
                            Toast.makeText(MainActivity.this, "User not Exist!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(MainActivity.this, "Failed to Connect Database!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }

    public void to_signup_page(View v)
    {
        txtSignUp=(TextView) findViewById(R.id.account);
        Intent signUp=new Intent(MainActivity.this,SignUp.class);
        startActivity(signUp);
    }

}
