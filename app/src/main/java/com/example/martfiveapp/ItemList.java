package com.example.martfiveapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import com.google.android.material.navigation.NavigationView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.widget.Toolbar;

import com.example.martfiveapp.Interface.ItemClickListener;
import com.example.martfiveapp.Model.Item;
import com.example.martfiveapp.ViewHolder.ItemViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ItemList extends AppCompatActivity  {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference itemList;

    String categoryId="";

    FirebaseRecyclerAdapter<Item, ItemViewHolder> adaptor;
    List<String> suggestList = new ArrayList<>();
    MaterialSearchBar materialSearchBar;


    //searchFuntionality
    FirebaseRecyclerAdapter<Item, ItemViewHolder> searchAdaptor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        //Firebase
        database = FirebaseDatabase.getInstance();
        itemList = database.getReference("Items");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_item);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //Get Intent here
        if(getIntent()!= null)
            categoryId = getIntent().getStringExtra("CategoryId");
        if(!categoryId.isEmpty() && categoryId != null)
        {
            loadListItem(categoryId);
        }

        materialSearchBar = (MaterialSearchBar)findViewById(R.id.searchBar);
        materialSearchBar.setHint("Enter you Item");
        
        loadSuggest(); //Write function to load Suggest form
        materialSearchBar.setLastSuggestions(suggestList);
        materialSearchBar.setCardViewElevation(10);
        materialSearchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //When user type their text, we will change suggest list

                List<String> suggest = new ArrayList<String>();
                for(String search:suggestList)
                {
                    if(search.toLowerCase().contains(materialSearchBar.getText().toLowerCase()))
                        suggest.add(search);
                }
                materialSearchBar.setLastSuggestions(suggest);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        materialSearchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {
                //When Search Bar is close Restore original adaptor
                if(!enabled)
                    recyclerView.setAdapter(adaptor);
            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                //When search finish show result of adaptor
                startSearch(text);
            }

            @Override
            public void onButtonClicked(int buttonCode) {

            }
        });
    }

    private void startSearch(CharSequence text) {
        FirebaseRecyclerOptions<Item> options = new FirebaseRecyclerOptions.Builder<Item>()
                .setQuery(itemList.orderByChild("Name").equalTo(text.toString())
                        , Item.class)
                .build();
        searchAdaptor = new FirebaseRecyclerAdapter<Item, ItemViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull ItemViewHolder itemViewHolder, int i, @NonNull Item item) {
                itemViewHolder.item_name.setText(item.getName());
                itemViewHolder.item_price.setText("Rs "+item.getPrice());
                itemViewHolder.item_quantity.setText(item.getQuantity());
                Picasso.with(getBaseContext()).load(item.getImage()).into(itemViewHolder.item_image);
                final Item clickItem = item;

                itemViewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Intent ItemDetail = new Intent(ItemList.this, Item_Detail.class);
                        ItemDetail.putExtra("ItemId", searchAdaptor.getRef(position).getKey());
                        startActivity(ItemDetail);
                    }
                });
            }

            @NonNull
            @Override
            public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);
                return new ItemViewHolder(view);
            }
        };
        recyclerView.setAdapter(searchAdaptor);
        searchAdaptor.startListening();
    }


    private void loadSuggest() {
        itemList.orderByChild("CategoryId").equalTo(categoryId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot postSnapshot:dataSnapshot.getChildren())
                        {
                            Item item = postSnapshot.getValue(Item.class);
                            suggestList.add(item.getName());
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void loadListItem(String categoryId) {

        FirebaseRecyclerOptions<Item> options = new FirebaseRecyclerOptions.Builder<Item>()
                .setQuery(itemList.orderByChild("CategoryId").equalTo(categoryId)
                        , Item.class)
                .build();
        adaptor = new FirebaseRecyclerAdapter<Item, ItemViewHolder>(options) {
            @Override
            protected void onBindViewHolder(ItemViewHolder holder,int position, Item model) {

                holder.item_name.setText(model.getName());
                holder.item_price.setText("Rs "+model.getPrice());
                holder.item_quantity.setText(model.getQuantity());
                Picasso.with(getBaseContext()).load(model.getImage()).into(holder.item_image);
                final Item clickItem = model;

                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Intent ItemDetail = new Intent(ItemList.this, Item_Detail.class);
                        ItemDetail.putExtra("ItemId", adaptor.getRef(position).getKey());
                        startActivity(ItemDetail);
                    }
                });
            }

            @Override
            public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);
                return new ItemViewHolder(view);
            }
        };
        recyclerView.setAdapter(adaptor);
        adaptor.startListening();
    }
}
